package com.example.wang.myapplication;

import android.app.DownloadManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetAddress;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {
    TextView responseText;
    String reponsedata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.button);
        responseText = findViewById(R.id.textView);
        //button.setOnClickListener(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("aaaa");
                Log.d("eee","www");
                responseText.setText(reponsedata);
                sendRequestWithHttpURLConnection();
            }
        });

    }
    //

    private void sendRequestWithHttpURLConnection() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();

                    //GET BY ID
                    //getbyid http://127.0.0.1:8082/demo/superadmin/getregisterbyid?registerId=1
                    //REMOVE BY ID
                    //http://127.0.0.1:8082/demo/superadmin/removeregister?registerID=1
              /*    Request request = new Request.Builder().url("http://130.56.231.9:8082/demo/superadmin/listregister").build();
                    Response response = client.newCall(request).execute();
                    String reponsedata = response.body().string();
                    System.out.println(reponsedata);
                    showResponse(reponsedata);*/
                     // parserJSONWithJSONObject(reponsedata);

                    // POST add register
                    InetAddress addr = InetAddress.getLocalHost();
                    //130.56.213.174 ipconfig ipv4
                    String localhost =addr.getHostAddress().toString();
                    String url = "http://"+localhost+":8082/demo/superadmin/addregister";
                    System.out.println(url);


                FormBody builder = new FormBody.Builder()
                        .add("registerId","1")
                        .add("registerEmail", "u23")
                        .add("registerPassword", "news")
                        .add("registerAddress", "20")
                        .add("registerName", "0")
                        .add("registerPhone", "1")
                        .add("registerRole","ww")
                        .build();
                Request requestPost = new Request.Builder()
                        .url("http://130.56.93.106:8082/demo/superadmin/modifyregister")
                        .post(builder)
                        .build();
                Response response = client.newCall(requestPost).execute();
                String reponsedata = response.body().string();
                System.out.println(reponsedata);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void showResponse(final String reponsedata) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                responseText.setText(reponsedata);
                System.out.println(reponsedata);
            }
        });
    }

}
